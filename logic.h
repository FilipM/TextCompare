#ifndef LOGIC_H
#define LOGIC_H

#include <vector>
#include <map>
#include <QString>
#include <QTextBrowser>

struct Location{
   QString location;
};

int process(QString &firstText, QString &secondText, QTextBrowser *textBrowser, Location *loc);

void getWordsFromText(std::vector<QString> &word_vector, QString text, std::vector<QString> &stop_Words,
                      std::vector<QString> &textWordCount);

void countWords(std::map<QString, int> &word_list, std::vector<QString> words);

std::vector<QString> *stop_word_loader(const QString location);

#endif // LOGIC_H
