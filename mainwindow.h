#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "logic.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    Location *loc;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_actionAbout_triggered();

    void on_actionE_xit_triggered();

    void on_action_Set_stop_word_list_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
