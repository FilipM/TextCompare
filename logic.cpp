#include "logic.h"
#include <vector>
#include <map>
#include <QString>
#include <QDebug>
#include <iostream>
#include <QFile>
#include <QTextBrowser>
#include <cmath>

using namespace std;

int process(QString &firstText, QString &secondText, QTextBrowser *textBrowser, Location *loc){
    vector<QString> *stop_Words = stop_word_loader(loc->location);
    if(stop_Words == nullptr){
        textBrowser->setText("Stop words list not found!\n"
                             "Add one using Change/Add stop word list button in the toolbar or file menu");
        return 0;
    }

    vector<QString> words;

    vector<QString> words_fromFirstText;
    vector<QString> words_fromSecondText;

    getWordsFromText(words, firstText, *stop_Words, words_fromFirstText);
    getWordsFromText(words, secondText, *stop_Words, words_fromSecondText);

    map<QString, int> text1;
    map<QString, int> text2;
    for(auto &it : words){
        text1.insert(pair<QString, int>(it, 0));
        text2.insert(pair<QString, int>(it, 0));
    }

    countWords(text1, words_fromFirstText);
    countWords(text2, words_fromSecondText);

    qDebug() << "Tekst 1" << endl;
    for(auto a : text1){
        qDebug() << a;
    }
    qDebug() << "Tekst 2" << endl;
    for(auto a : text2){
        qDebug() << a;
    }

    int SSD = 0;

    double brojIstihRijeci = 0;
    double brojRijeci1 = 0;
    double brojRijeci2 = 0;

    for(auto &iter : words){
        int sum = text1[iter] - text2[iter];
        SSD += static_cast<int>(pow(sum, 2.0));

        if(text1[iter] != 0 && text2[iter] != 0) brojIstihRijeci++;
        if(text1[iter] > 0) brojRijeci1++;
        if(text2[iter] > 0) brojRijeci2++;

    }

    double Jaccard = static_cast<double>(brojIstihRijeci / (brojRijeci1 + brojRijeci2 - brojIstihRijeci));

    textBrowser->setText("SSD = " + QString::number(SSD));
    textBrowser->append("Jaccard(A, B) = " + QString::number(Jaccard));

    delete stop_Words;

    return 0;
}

void getWordsFromText(vector<QString> &word_vector, QString text, vector<QString> &stop_Words,
                      vector<QString> &textWordCount){
    QString word;
    for(int i = 0; i < text.length(); ++i){
        if((text[i] >= 'a' && text[i] <= 'z') || (text[i] >= 'A' && text[i] <= 'Z') || (text[i] >= '0' && text[i] <= '9')
                || text[i] == '\''){
            text[i] = text[i].toLower();
            word += text[i];
        }

        if(((text[i] < 'a' || text[i] > 'z') && (text[i] < 'A' || text[i] > 'Z')
                && (text[i] != '\'')) ||
                text[i] == ' ' || text[i] == '.' || text[i] == ',' || text[i] == '(' ||
                text[i] == ')' || text[i] == '<' || text[i] == '>' ||
                text[i] == '\n' || text[i] == '\r' ||
                text[i] == ':' || text[i] == '@' || text[i] == '\t'
                || i == text.length() - 1){
            if(!word.isEmpty()){
                bool stopW = true;

                for(auto &stp_word : stop_Words){
                    if(stp_word == word)
                        stopW = false;
                }

                if(stopW != false)
                    textWordCount.push_back(word);

                for(auto &vect_word : word_vector){
                    if(vect_word == word)
                        stopW = false;
                }

                if(stopW == true){
                    word_vector.push_back(word);
                    word.clear();
                }
                else word.clear();
        }

        /*if(text[i] == ' ' || text[i] == '.' || text[i] == ',' || text[i] == '(' ||
                text[i] == ')' || text[i] == '<' || text[i] == '>' ||
                text[i] == '\n' || text[i] == '\r' ||
                text[i] == ':' || text[i] == '@' || text[i] == '\t'
                || i == text.length() - 1){
            if(!word.isEmpty()){
                bool stopW = true;

                for(auto &stp_word : stop_Words){
                    if(stp_word == word)
                        stopW = false;
                }

                if(stopW != false)
                    textWordCount.push_back(word);

                for(auto &vect_word : word_vector){
                    if(vect_word == word)
                        stopW = false;
                }

                if(stopW == true){
                    word_vector.push_back(word);
                    word.clear();
                }
                else word.clear();
            }*/
        }
    }
}

void countWords(map<QString, int> &word_list, vector<QString> words){
    for(auto &word : words){
        map<QString, int>::iterator iter = word_list.find(word);
        iter->second++;
    }
}

vector<QString> *stop_word_loader(const QString location){
    auto *words = new vector<QString>;
    QString word;

    QFile stopwords_file(location);
    if(!stopwords_file.open(QIODevice::ReadOnly | QIODevice::Text)){
        cerr << "Stop word list not found!" << endl << "Check your directory" << endl;
        return nullptr;
    }

    QTextStream in(&stopwords_file);
    while(!in.atEnd()){
        QString word = in.readLine();
        words->push_back(word);
    }

    return words;
}
