#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "logic.h"
#include <QTextEdit>
#include <QDebug>
#include <QLabel>
#include <QTextBrowser>
#include <QCoreApplication>
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    loc = new Location;
    loc->location = "Lab05_stop-word-list.txt";
}

MainWindow::~MainWindow()
{
    delete loc;
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString text1 = ui->textEdit->toPlainText();
    QString text2 = ui->textEdit_2->toPlainText();

    if(text1.isEmpty()){
        ui->textBrowser->setText("You need to insert some text to first textbox!\n");
    }else if(text2.isEmpty()){
        ui->textBrowser->append("You need to insert some text to second textbox!");
    }else{
        ui->textBrowser->setText(text1);
        process(text1, text2, ui->textBrowser, loc);
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::information(this, tr("About"), tr("Made in Qt5\n" "Built using Qt version 5.11"), QMessageBox::Close);
}

void MainWindow::on_actionE_xit_triggered()
{
    QCoreApplication::quit();
}

void MainWindow::on_action_Set_stop_word_list_triggered()
{
    loc->location = QFileDialog::getOpenFileName(this,
                                                      tr("Select stop words list"), "", tr("Text files (*.txt)"));
}
